package com.swelabs.tanksrpg;

import com.swelabs.tanksrpg.commands.*;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Events {

    @EventSubscriber
    public void onReadyEvent(ReadyEvent ready_event) {
        ready_event.getClient().changePlayingText(">help\tto start!");
        System.out.println("Bot has started successfully!");
    }

    @EventSubscriber
    public void onMessageRecieved(MessageReceivedEvent event) {

        String[] argArray = event.getMessage().getContent().split(" "); //Splits the array into pieces
        String usrName = event.getMessage().getAuthor().getName(); // gets the users name, for debugging purposes
        long usrId = event.getMessage().getAuthor().getLongID(); // gets the users ID in a long, for debugging purposes

        if (argArray.length == 0)
            return;

        if (!argArray[0].startsWith(Command.prefix))
            return;

        String command = argArray[0].substring(1); // The command

        System.out.println("DEBUG: " + usrName + " (" + usrId + "): " + command);

        List<String> argsList = new ArrayList<>(Arrays.asList(argArray)); // Turns the array into a list (safe usage)
        argsList.remove(0); //Removes the command and prefix

        switch (command.toLowerCase()) { // toLowerCase() Makes all the commands lowercase
            case "ping":
                ping.runCommand(event, argsList);
                break;

            case "help":
                help.runCommand(event, argsList);
                break;

            case "record":
                record.runCommand(event, argsList);
                break;

            case "tank":
                tank.runCommand(event, argsList);
                break;

            case "garage":
                garage.runCommand(event, argsList);
                break;
            case "tanks":
                tanks.runCommand(event, argsList);
                break;
            case "lootcrate":
                lootcrate.runCommand(event, argsList);
                break;
        }





    }

    // Commands
    private static Ping ping = new Ping();
    private static Record record = new Record();
    private static Tank tank = new Tank();
    private static Help help = new Help();
    private static Garage garage = new Garage();
    private static Tanks tanks = new Tanks();
    private static Lootcrate lootcrate = new Lootcrate();
}
