package com.swelabs.tanksrpg;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Table;
import com.rethinkdb.net.Connection;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Cooldown {

    // Initializes the database
    private static final RethinkDB r = RethinkDB.r;

    // Connects to the database
    private static Connection conn = r.connection().connect();

    // Predefined tables
    private static Table coolDown = r.db("tanksrpg").table("cooldown");

    public static boolean onCoolDown(long userID, String command, long milliSeconds) {

        // Creates the cooldown entry for the player
        if (coolDown.get(userID).run(conn) == null) {

            // If the player ID doesn't exist in cooldown database..
            coolDown.insert(r.hashMap("id", userID).with(command, System.currentTimeMillis())).run(conn);
        } else if (coolDown.get(userID).hasFields(command).run(conn).equals(false)) {

            // If the player ID exists, but not the command cooldown.
            coolDown.update(r.hashMap(command, System.currentTimeMillis())).run(conn);
        }
        long time = System.currentTimeMillis(); // Current UNIX time
        long cooldown = coolDown.get(userID).g(command).run(conn); // The UNIX time stored in the database

        // If the time is greater than the cooldown time in database, add the cooldown, otherwise, don't
        if (time > cooldown) {
            addCooldown(milliSeconds, userID, command); // Adds the cooldown to the user and command
        }
        return time < cooldown; // If the current time is less than the cooldown time
    }

    public static long coolDownTime(long userID, String command) {

        // Retrives the time left on cooldown
        long cooldown = coolDown.get(userID).g(command).run(conn);
        long time = System.currentTimeMillis();
        long timeLeft = (cooldown - time) / 1000;

        return timeLeft;
    }

    private static void addCooldown(long milliSeconds, long userID, String command) {

        // Adds cooldown to the player
        coolDown.get(userID).update(r.hashMap(command, System.currentTimeMillis() + milliSeconds)).run(conn);
    }
}
