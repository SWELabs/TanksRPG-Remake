package com.swelabs.tanksrpg;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public interface Command {

    // Cooldown in milliseconds
    int cooldown = 0;

    // Name of the command
    String name = "";

    // The bots prefix
    String prefix = ">";

    // Interface for a command to be implemented
    void runCommand(MessageReceivedEvent event, List<String> argsList);


}
