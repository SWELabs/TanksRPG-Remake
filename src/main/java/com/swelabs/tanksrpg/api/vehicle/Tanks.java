package com.swelabs.tanksrpg.api.vehicle;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Table;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;

import java.util.List;
import java.util.Objects;

public class Tanks {

    // Defines rethinkdb (database)
    private static final RethinkDB r = RethinkDB.r;

    // Connects to the database
    private static Connection conn = r.connection().connect();

    // The vehicle database, i.e tanks.
    private static Table tanks = r.db("tanksrpg").table("vehicles");


    public String showTankName(String tankID) {
        if (tankID == tanks.get(tankID).g("id").run(conn)) {
            return tanks.get(tankID).g("name").run(conn);
        }
        return "Tank not found";
    }

    public static String showTank(String tankName, String userName) {

        // If the tank name is an ID...
        if (tanks.get(tankName).run(conn) != null) {
            String id = tanks.get(tankName).g("id").run(conn); // The tank's ID
            String name = tanks.get(tankName).g("name").run(conn); // The tanks name
            long rank = tanks.get(tankName).g("rank").run(conn); // The tanks rank
            long health = tanks.get(tankName).g("health").run(conn); //The tanks health
            long armor = tanks.get(tankName).g("armor").run(conn); // The tanks armor
            long caliber = tanks.get(tankName).g("caliber").run(conn); // The tanks caliber
            long damage = tanks.get(tankName).g("damage").run(conn); // The tanks damage
            long penetration = tanks.get(tankName).g("penetration").run(conn); // The tanks penetration
            long xp = tanks.get(tankName).g("xp").run(conn); // The xp needed to unlock the tank
            long cost = tanks.get(tankName).g("cost").run(conn); // Silver/gold crowns needed to buy the tank
            boolean event = tanks.get(tankName).g("event").run(conn); // If the tank is an event tank or standard tank

            String event_tank = String.format("```\nType: Event Tank\nName: %s\nID: %s\nRank: %s\nHealth: %s\nArmor: %smm\nCaliber: %smm\nDamage: %s\nPenetration: %smm\nCost: %s gold crowns\n```", name, id, rank, health, armor, caliber, damage, penetration, cost);
            String normal_tank = String.format("```\nType: Standard Tank\nName: %s\nID: %s\nRank: %s\nHealth: %s\nArmor: %smm\nCaliber: %smm\nDamage: %s\nPenetration: %smm\nXP to unlock: %s\nCost: %s silver crowns```", name, id, rank, health, armor, caliber, damage, penetration, xp, cost);

            // If the tank is a special tank or not
            if (event) {
                return event_tank;
            } else {
                return normal_tank;
            }
        }

        // Looks for the tank name in the database
        if (tanks.filter(r.hashMap("name", tankName.toLowerCase())).g("name").isEmpty().run(conn))  {

            // If tank isn't found, return this:
            return String.format("Sorry, %s, that tank does not exist or you typed something wrong.", userName);

        } else {
            Cursor tankID = tanks.filter(r.hashMap("name", tankName.toLowerCase())).getField("id").run(conn);
            String id = (String) tankID.next(); // The tank's ID
            String name = tanks.get(id).g("name").run(conn); // The tanks name
            long rank = tanks.get(id).g("rank").run(conn); // The tanks rank
            long health = tanks.get(id).g("health").run(conn); //The tanks health
            long armor = tanks.get(id).g("armor").run(conn); // The tanks armor
            long caliber = tanks.get(id).g("caliber").run(conn); // The tanks caliber
            long damage = tanks.get(id).g("damage").run(conn); // The tanks damage
            long penetration = tanks.get(id).g("penetration").run(conn); // The tanks penetration
            long xp = tanks.get(id).g("xp").run(conn); // The xp needed to unlock the tank
            long cost = tanks.get(id).g("cost").run(conn); // Silver/gold crowns needed to buy the tank
            boolean event = tanks.get(id).g("event").run(conn); // If the tank is an event tank or standard tank

            String event_tank = String.format("```\nType: Event Tank\nName: %s\nID: %s\nRank: %s\nHealth: %s\nArmor: %smm\nCaliber: %smm\nDamage: %s\nPenetration: %smm\nCost: %s gold crowns\n```", name, id, rank, health, armor, caliber, damage, penetration, cost);
            String normal_tank = String.format("```\nType: Standard Tank\nName: %s\nID: %s\nRank: %s\nHealth: %s\nArmor: %smm\nCaliber: %smm\nDamage: %s\nPenetration: %smm\nXP to unlock: %s\nCost: %s silver crowns```", name, id, rank, health, armor, caliber, damage, penetration, xp, cost);

            // If the tank is a special tank or not
            if (event) {
                return event_tank;
            } else {
                return normal_tank;
            }
        }
    }
}
