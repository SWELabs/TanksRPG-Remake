package com.swelabs.tanksrpg.api.vehicle;

public class Vehicle {

    //Initializes all variables
    String name;
    String id;
    int rank;
    int health;
    int armor;
    int caliber;
    int damage;
    int penetration;
    int xp;
    int cost;
    boolean event;

    public Vehicle(String name, String id, int rank, int health, int armor, int caliber, int damage, int penetration, int xp, int cost, boolean event) {

        //Creates a template for a vehicle
        this.name = name;
        this.id = id;
        this.rank = rank;
        this.health = health;
        this.armor = armor;
        this.caliber = caliber;
        this.damage = damage;
        this.penetration = penetration;
        this.xp = xp;
        this.cost = cost;
        this.event = event;
    }

}
