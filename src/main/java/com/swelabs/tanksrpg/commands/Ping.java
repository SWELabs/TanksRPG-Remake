package com.swelabs.tanksrpg.commands;

import com.swelabs.tanksrpg.Command;
import com.swelabs.tanksrpg.Cooldown;
import com.swelabs.tanksrpg.Utility;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Ping implements Command {

    // Command name
    private String name = "ping";

    // Command cooldown in milliseconds
    private int cooldown = 5000;


    @Override
    public void runCommand(MessageReceivedEvent event, List<String> argsList) {

        // Fetches the users id
        long userID = event.getAuthor().getLongID();

        //Ping responds to choose from
        String responds[] = {
                "The weight of the air is torture.",
                "Time to make my exit from this fairytale.",
                "Sayonara~",
                "My departure was foreseen.",
                "Hello, nice to meet you.. You seem familiar, have I met you before?",
                "Insanity.",
                "Psychopathy.",
                "The illusion of ignorance.",
                "Captivity.",
                "Try to stop it from corrupting...",
                "The days have turned into nights.",
                "Darkness has consumed the light.",
                "Goodbye sweetie, nice to see you.",
                "My heart...",
                "Why won't anyone notice the torment..?",
                "This madness is causing...-",
                "Terror of my own self-concious mind's persecution.",
                "I won't survive like this...",
                "Sanity.",
                "Light is peaking through the darkness.",
                "Purity.",
                "Sanity, it's already fading away...",
                "Assum life of insanity~",
                "Everything is said and done...",
                "Everyone has had their fun.",
                "Legends never die... They become a part of you.",
                "Shit! The covenant is coming, members of UNSC, prepare for battle! Frigates, and Pillar of Autumn, fire at will!"
        };

        // Fetches the length and makes it a random number
        int randNumber = ThreadLocalRandom.current().nextInt(0, responds.length);



        try {
            // Checks if the command is on cooldown or not
            if (Cooldown.onCoolDown(userID, name, cooldown)) {
                Utility.sendMessage(event.getChannel(), String.format("Please wait for cooldown to end. (%ss)", Cooldown.coolDownTime(userID, name)));
            } else {
                Utility.sendMessage(event.getChannel(), responds[randNumber]);
            }
        } catch (Exception e) { // Catches any occurring exceptions
            Utility.sendMessage(event.getChannel(),"Exception caught!\n```diff\n- " + e + "\n```\n Please report this to `Damien Moon#5773`");
        }
    }
}
