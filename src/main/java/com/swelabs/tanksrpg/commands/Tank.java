package com.swelabs.tanksrpg.commands;

import com.swelabs.tanksrpg.Command;
import com.swelabs.tanksrpg.Cooldown;
import com.swelabs.tanksrpg.Utility;
import com.swelabs.tanksrpg.api.vehicle.Tanks;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;
import java.util.regex.Pattern;

public class Tank implements Command {

    // Command name
    private String name = "tank";

    // Command cooldown in milliseconds
    private int cooldown = 5000;
    @Override
    public void runCommand(MessageReceivedEvent event, List<String> argsList) {
        String tankName = String.join(" ", argsList); // Turns the args into a single string with spaces
        String userName = event.getAuthor().getName(); // Fetches the authors name
        long userID = event.getAuthor().getLongID(); // Fetches the authors ID

        // --------------------------------------------------------------------------- Tokenize

        //String[] tokens = tokenIze(tankName);
        // ---------------------------------------------------------------------------

        try {
            // Checks if the command is on cooldown or not
            if (Cooldown.onCoolDown(userID, name, cooldown)) {
                Utility.sendMessage(event.getChannel(), String.format("Please wait for cooldown to end. (%ss)", Cooldown.coolDownTime(userID, name)));
            } else {
                Utility.sendMessage(event.getChannel(), Tanks.showTank(tankName, userName));
            }
        } catch (Exception e) { // Catches any occurring exceptions
            Utility.sendMessage(event.getChannel(),"Exception caught!\n```diff\n- " + e + "\n```\n Please report this to `Damien Moon#5773`");
        }
    }

    /*public String[] tokenIze(String inputString) {

        // Normalize Data
        String normalString = inputString.toUpperCase();    // convert letter casing
        normalString = inputString.trim();                  // Remove leading an trailing spacing (if any)

        Pattern normalPattern = Pattern.compile("([^\\s])");
        normalString = normalPattern.matcher(normalString).replaceAll()

        // resultant
        String[] tokenList = new String[30];    // Set a max Token of 30 for now
        return tokenList;
    }*/
}
