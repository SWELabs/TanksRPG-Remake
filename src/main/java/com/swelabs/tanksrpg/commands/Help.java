package com.swelabs.tanksrpg.commands;

import com.swelabs.tanksrpg.Command;
import com.swelabs.tanksrpg.Cooldown;
import com.swelabs.tanksrpg.Utility;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class Help implements Command {

    // Command name
    private String name = "help";

    // Command cooldown
    private int cooldown = 5000;

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> argsList) {

        String commands = "```diff\n"
                + "<-- List of commands -->\n"
                + "ping - Responds with 'Pong!'\n"
                + "help - Shows the help message.\n"
                + "record - Shows your battle record.\n"
                + "tank <tank name> - Shows the specifications of a tank.\n"
                + "garage - Views the tanks you currently own.\n"
                + "tanks - Views all available tanks in the game.\n"
                + "lootcrate - Gives you silver crowns and gold crowns daily.\n"
                + "(The default PREFIX is >)\n"
                + "```";

        // Fetches the users id
        long userID = event.getAuthor().getLongID();

        try {
            // Checks if the command is on cooldown or not
            if (Cooldown.onCoolDown(userID, name, cooldown)) {
                Utility.sendMessage(event.getChannel(), String.format("Please wait for cooldown to end. (%ss)", Cooldown.coolDownTime(userID, name)));
            } else {
                Utility.sendMessage(event.getChannel(), commands);
            }
        } catch (Exception e) { // Catches any occurring exceptions
            Utility.sendMessage(event.getChannel(),"Exception caught!\n```diff\n- " + e + "\n```\n Please report this to `Damien Moon#5773`");
        }
    }
}
