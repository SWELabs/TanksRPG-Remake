package com.swelabs.tanksrpg.commands;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Table;
import com.rethinkdb.net.Connection;
import com.swelabs.tanksrpg.Command;
import com.swelabs.tanksrpg.Cooldown;
import com.swelabs.tanksrpg.Utility;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Lootcrate implements Command {

    // This is a template, for how every command should look like.

    // Cooldown in milliseconds
    long cooldown = 86400000;

    // Name of the command
    String name = "lootcrate";

    // Initializes the database
    private static final RethinkDB r = RethinkDB.r;

    // Connects to the database
    Connection conn = r.connection().connect();

    // The player table
    Table players = r.db("tanksrpg").table("players");

    // The garage table
    Table garage = r.db("tanksrpg").table("garage");

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> argsList) {

        // Fetches the users ID
        long userID = event.getAuthor().getLongID();

        // Fetches the users name
        String userName = event.getAuthor().getName();

        try {
            // Checks if the command is on cooldown or not
            if (Cooldown.onCoolDown(userID, name, cooldown)) {
                Utility.sendMessage(event.getChannel(), String.format("Please wait for cooldown to end. (%s hours)", ((Cooldown.coolDownTime(userID, name) / 60) / 60)));
            } else {
                Utility.sendMessage(event.getChannel(), recieveLoot(userID, userName));
            }
        } catch (Exception e) { // Catches any occurring exceptions
            Utility.sendMessage(event.getChannel(),"Exception caught!\n```diff\n- " + e + "\n```\n Please report this to `Damien Moon#5773`");
        }
    }

    String recieveLoot(long userID, String userName) {

        // Checks if the player exists
        if (players.get(userID).run(conn) == null) {
            return userName + ", " + "you must create a record first by typing `>record`";
        }
        // The silver crowns to be generated
        int randomSilver = ThreadLocalRandom.current().nextInt(0, 1000 + 1);

        // The gold crowns to be generated
        int randomGold = ThreadLocalRandom.current().nextInt(0, 10 + 1);

        // The current silver of the player
        long currentSilver = players.get(userID).g("silver").run(conn);

        // The current gold of the player
        long currentGold = players.get(userID).g("gold").run(conn);

        // Random number to win a tank!
        int randNum = ThreadLocalRandom.current().nextInt(0, 100 + 1);
        if (randNum == 69) {
            // Checks if the player already has a tank
            if (garage.get(userID).g("vehicles").hasFields("012").run(conn)) {
                players.get(userID).update(r.hashMap("gold", currentGold + 500)).run(conn);
                return "You opened your daily lootcrate and got...\n`A crusader mk. 3`!\nBut you already own it, so you get 750 gold crowns instead.";
            }
            // Adds the tank to the player
            garage.get(userID).update(r.hashMap("vehicles", r.hashMap("012", 1))).run(conn);
            return "You opened your daily lootcrate and got...\nA `crusader mk. 3`!";
        } else if (randNum == 1) {
            players.get(userID).update(r.hashMap("silver", currentSilver - 5000)).run(conn);
            return "Aww... " + userName + " was unlucky and the lootcrate stole 5000 silver crowns!";
        }

        // Gives the player the loot
        players.get(userID).update(r.hashMap("silver", currentSilver + randomSilver).with("gold", currentGold + randomGold)).run(conn);
        return "You opened your daily lootcrate and got...\n" + randomSilver + " Silver Crowns!\n" + randomGold + " Golden Crowns!";
    }
}
