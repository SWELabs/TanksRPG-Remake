package com.swelabs.tanksrpg.commands;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Table;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;
import com.swelabs.tanksrpg.Command;
import com.swelabs.tanksrpg.Cooldown;
import com.swelabs.tanksrpg.Utility;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.ArrayList;
import java.util.List;

public class Tanks implements Command {

    // This is a template, for how every command should look like.

    // Cooldown in milliseconds
    int cooldown = 10000;

    // Name of the command
    String name = "tanks";

    // The database
    private static final RethinkDB r = RethinkDB.r;

    // Opens connection to DB
    Connection conn = r.connection().connect();

    // The tanks table
    Table tanks = r.db("tanksrpg").table("vehicles");


    @Override
    public void runCommand(MessageReceivedEvent event, List<String> argsList) {

        // Fetches the users ID
        long userID = event.getAuthor().getLongID();

        try {
            // Checks if the command is on cooldown or not
            if (Cooldown.onCoolDown(userID, name, cooldown)) {
                Utility.sendMessage(event.getChannel(), String.format("Please wait for cooldown to end. (%ss)", Cooldown.coolDownTime(userID, name)));
            } else {
                Utility.sendMessage(event.getChannel(), listAllTanks());
            }
        } catch (Exception e) { // Catches any occurring exceptions
            Utility.sendMessage(event.getChannel(),"Exception caught!\n```diff\n- " + e + "\n```\n Please report this to `Damien Moon#5773`");
        }
    }

    private String listAllTanks() {
        Cursor<String> rankOne = tanks.filter(r.hashMap("rank", 1)).getField("name").run(conn);
        Cursor<String> rankTwo = tanks.filter(r.hashMap("rank", 2)).getField("name").run(conn);
        Cursor<String> rankThree = tanks.filter(r.hashMap("rank", 3)).getField("name").run(conn);
        Cursor<String> rankFour = tanks.filter(r.hashMap("rank", 4)).getField("name").run(conn);
        Cursor<String> rankFive = tanks.filter(r.hashMap("rank", 5)).getField("name").run(conn);
        Cursor<String> rankSix = tanks.filter(r.hashMap("rank", 6)).getField("name").run(conn);
        Cursor<String> rankSeven = tanks.filter(r.hashMap("rank", 7)).getField("name").run(conn);
        Cursor<String> rankEight = tanks.filter(r.hashMap("rank", 8)).getField("name").run(conn);
        Cursor<String> rankNine = tanks.filter(r.hashMap("rank", 9)).getField("name").run(conn);
        Cursor<String> rankTen = tanks.filter(r.hashMap("rank", 10)).getField("name").run(conn);

        List<String> tanksOne = rankOne.toList();
        List<String> tanksTwo = rankTwo.toList();
        List<String> tanksThree = rankThree.toList();
        List<String> tanksFour = rankFour.toList();
        List<String> tanksFive = rankFive.toList();
        List<String> tanksSix = rankSix.toList();
        List<String> tanksSeven = rankSeven.toList();
        List<String> tanksEight = rankEight.toList();
        List<String> tanksNine = rankNine.toList();
        List<String> tanksTen = rankTen.toList();

        String nameOne = String.join("\n", tanksOne);
        String nameTwo = String.join("\n", tanksTwo);
        String nameThree = String.join("\n", tanksThree);
        String nameFour = String.join("\n", tanksFour);
        String nameFive = String.join("\n", tanksFive);
        String nameSix = String.join("\n", tanksSix);
        String nameSeven = String.join("\n", tanksSeven);
        String nameEight = String.join("\n", tanksEight);
        String nameNine = String.join("\n", tanksNine);
        String nameTen = String.join("\n", tanksTen);

        return String.format("```\nRank 1:\n%s\n\nRank 2:\n%s\n\nRank 3:\n%s\n\nRank 4:\n%s\n\nRank 5:\n%s\n\nRank 6:\n%s\n\nRank 7:\n%s\n\nRank 8:\n%s\n\nRank 9:\n%s\n\nRank 10:\n%s\n\n```", nameOne, nameTwo, nameThree, nameFour, nameFive, nameSix, nameSeven, nameEight, nameNine, nameTen);
    }
}