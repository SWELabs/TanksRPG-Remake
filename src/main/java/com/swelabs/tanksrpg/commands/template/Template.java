package com.swelabs.tanksrpg.commands.template;

import com.swelabs.tanksrpg.Command;
import com.swelabs.tanksrpg.Cooldown;
import com.swelabs.tanksrpg.Utility;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class Template implements Command {

    // This is a template, for how every command should look like.

    // Cooldown in milliseconds
    int cooldown = 0;

    // Name of the command
    String name = "";


    @Override
    public void runCommand(MessageReceivedEvent event, List<String> argsList) {

        // Fetches the users ID
        long userID = event.getAuthor().getLongID();

        try {
            // Checks if the command is on cooldown or not
            if (Cooldown.onCoolDown(userID, name, cooldown)) {
                Utility.sendMessage(event.getChannel(), String.format("Please wait for cooldown to end. (%ss)", Cooldown.coolDownTime(userID, name)));
            } else {
                Utility.sendMessage(event.getChannel(), "Do stuff here");
            }
        } catch (Exception e) { // Catches any occurring exceptions
            Utility.sendMessage(event.getChannel(),"Exception caught!\n```diff\n- " + e + "\n```\n Please report this to `Damien Moon#5773`");
        }
    }
}
