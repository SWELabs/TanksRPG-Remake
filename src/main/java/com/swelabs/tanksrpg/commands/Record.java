package com.swelabs.tanksrpg.commands;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Table;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Util;
import com.swelabs.tanksrpg.Command;
import com.swelabs.tanksrpg.Utility;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import com.swelabs.tanksrpg.Cooldown;

import java.util.List;

public class Record implements Command {

    // Command cooldown in milliseconds
    private int cooldown = 10000;

    private String name = "record";

    // Initializes RethinkDB
    private static final RethinkDB r = RethinkDB.r;

    // Open a connection with RethinkDB
    private Connection conn = r.connection().connect();

    // Predefined tables
    private Table players = r.db("tanksrpg").table("players");
    private Table tanks = r.db("tanksrpg").table("tanks");

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> argsList) {

        // Defines the users id and name
        String userName = event.getAuthor().getName();
        long userID = event.getAuthor().getLongID();

        // Checks if the user exists in the database
        try {
            // The cooldown function
            if (Cooldown.onCoolDown(userID, name, cooldown)) {
                Utility.sendMessage(event.getChannel(), String.format("Please wait for cooldown to end. (%ss)", Cooldown.coolDownTime(userID, name)));

            } else {
                if (checkIfUserExists(userID)) {
                    Utility.sendMessage(event.getChannel(), showUser(userID, userName));
                } else {
                    Utility.sendMessage(event.getChannel(), createUser(userID, userName));
                }
            }
        } catch (Exception e) { // Catches any occurring exceptions
            Utility.sendMessage(event.getChannel(),"Exception caught!\n```diff\n- " + e + "\n```\n Please report this to `Damien Moon#5773`");
        }
    }

    private String createUser(long userID, String userName) {
        // Creates the user in the database
        players.insert(
                r.hashMap("id", userID)
                        .with("battles", 0)
                        .with("gold", 0)
                        .with("silver", 10000)
                        .with("experience", 0)
                        .with("ammo", r.hashMap("ap", 50)
                                .with("he", 50))).run(conn);
        /* The above adds the following to a player (example):
            ID: 12345780770
            Battles: 0
            Gold: 0
            Silver: 0
            Experience: 0
            Ammo:
                ap: 50
                he: 50
         */
        return "Created a record for " + userName + "!";
    }

    private String showUser(long userID, String userName) {
        // Fetches the users information from the database

        // turn all values into variables from the database
        long battles = players.get(userID).g("battles").run(conn);
        long gold_crowns = players.get(userID).g("gold").run(conn);
        long silver_crowns = players.get(userID).g("silver").run(conn);
        long experience = players.get(userID).g("experience").run(conn);
        long ap = players.get(userID).g("ammo").g("ap").run(conn);
        long he = players.get(userID).g("ammo").g("he").run(conn);

        // Returns the players record
        return "```\n<-- " + userName + "'s record -->"
                + "\nBattles: " + battles
                + "\nGold Crowns: " + gold_crowns
                + "\nSilver Crowns: " + silver_crowns
                + "\nExperience: " + experience
                + "\n\nAmmo:\nArmor Piercing: " + ap
                + "\nHigh Explosive: " + he
                + "\n```\n`>garage` to view your tanks.";

    }

    private boolean checkIfUserExists(long userID) {
        // Check if the user's id is in the database or not
        return players.get(userID).run(conn) != null;
    }
}
