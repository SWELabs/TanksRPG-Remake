package com.swelabs.tanksrpg.commands;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Table;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;
import com.swelabs.tanksrpg.Command;
import com.swelabs.tanksrpg.Cooldown;
import com.swelabs.tanksrpg.Utility;
import com.swelabs.tanksrpg.api.vehicle.Tanks;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.*;

public class Garage implements Command {

    // This is a template, for how every command should look like.

    // Cooldown in milliseconds
    int cooldown = 10000;

    // Name of the command
    String name = "garage";

    // Initializes the database
    private static final RethinkDB r = RethinkDB.r;
    Connection conn = r.connection().connect();

    // Garage database table
    private Table garage = r.db("tanksrpg").table("garage");

    // Vehicle database table
    private Table tanks = r.db("tanksrpg").table("vehicles");

    @Override
    public void runCommand(MessageReceivedEvent event, List<String> argsList) {

        // Fetches the users ID
        long userID = event.getAuthor().getLongID();

        // Fetches the users name
        String userName = event.getAuthor().getName();

        try {
            // Checks if the command is on cooldown or not
            if (Cooldown.onCoolDown(userID, name, cooldown)) {
                Utility.sendMessage(event.getChannel(), String.format("Please wait for cooldown to end. (%ss)", Cooldown.coolDownTime(userID, name)));
            } else {
                Utility.sendMessage(event.getChannel(), viewGarage(userID, userName));
            }
        } catch (Exception e) { // Catches any occurring exceptions
            Utility.sendMessage(event.getChannel(),"Exception caught!\n```diff\n- " + e + "\n```\n Please report this to `Damien Moon#5773`");
        }
    }

    // Views the players garage
    private String viewGarage(long userID, String userName) {
        if (garage.get(userID).run(conn) == null) {
            createGarage(userID);
        }
        // Fetches the players garage in a string
        ArrayList<String> playergarage = garage.get(userID).g("vehicles").keys().run(conn);

        // Loops through the players garage and returns the names
        for (int i = 0; i < playergarage.size(); i++) {
            if (Objects.equals(playergarage.get(i), tanks.get(playergarage.get(i)).g("id").run(conn).toString())) {
                playergarage.set(i, tanks.get(playergarage.get(i)).g("name").run(conn));
            }
        }

        // If something faulty happened, rip
        if (garage.get(userID).run(conn) == null) {
            return "You have no tanks in your garage, " + userName + ".\n(Report this to Damien Moon#5773)";
        }
        return String.format("```\n%s's Garage:\n\n%s\n```", userName, String.join("\n", playergarage));
    }

    // Creates the players garage
    private void createGarage(long userID) {
        // Creates the user in the database
        garage.insert(r.hashMap("id", userID).with("selected", null)).run(conn);
        garage.get(userID).update(r.hashMap("vehicles", r.hashMap("001", 1))).run(conn);
    }
}
